//  Import mongoJS
const mongoose = require('mongoose');

//  Khai báo Schema từ thư viện mongoose
const Schema = mongoose.Schema;

//  Khởi tạo 1 schema với các thuộc tính được yêu cầu
const courseSchema = new Schema({
    _id: {
        type: mongoose.Types.ObjectId,
    },
    courseCode: {
        type: String,
        unique: true,
        required: true
    },
    courseName: {
        type: String,
        required: true
    },
    price: {
        type: Number,
        default: 0
    },
    discountPrice: {
        type: Number,
        default: 0
    },
    duration: {
        type: String,
        required: true
    },
    level: {
        type: String,
        required: true
    },
    coverImage: {
        type: String,
        required: true
    },
    teacherName: {
        type: String,
        required: true
    },
    teacherPhoto: {
        type: String,
        required: true
    },
    isPopular: {
        type: Boolean,
        default: true
    },
    isTrending: {
        type: Boolean,
        default: false
    },
    ngayTao: {
        type: Date,
        default: Date.now()
    },
    ngayCapNhat: {
        type: Date,
        default: Date.now()
    },
    reviews: [{
        type: mongoose.Types.ObjectId,
        ref: "review"
    }]
});

//  B4:  Export ra 1 model nhờ Schema vừa khai báo
module.exports = mongoose.model("course", courseSchema);
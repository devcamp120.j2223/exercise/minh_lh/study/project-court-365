//  Khai báo thư viện express
const express = require('express');

//Import courseMiddleware
// const reviewMiddleware = require('../middleware/reviewMiddleware');

//Import course Controller
const { createReviewOfCourse, getAllReviewOfCourse, getReviewById, updateReviewById, deleteReviewById } = require('../controllers/reviewController');

//tạo router
const reviewRouter = express.Router();

//sủ dụng middle ware
// reviewRouter.use(reviewMiddleware);

reviewRouter.post('/courses/:courseId/reviews', createReviewOfCourse);

reviewRouter.get('/courses/:courseId/reviews', getAllReviewOfCourse);

reviewRouter.get('/review/:reviewId', getReviewById);

reviewRouter.put('/review/:reviewId', updateReviewById);

reviewRouter.delete('/courses/:courseId/reviews/:reviewId', deleteReviewById);


//Export
module.exports = { reviewRouter };
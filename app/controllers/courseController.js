//  Import course model vào controllers
const courseModel = require("../models/courseModel");

//Khai báo thư viện mongoose
const mongoose = require('mongoose');


//Create
const createCourse = (request, response) => {

    //B1: Thu thập dữ liệu
    let bodyRequest = request.body;

    //B2: Kiểm tra dữ liệu
    if (!bodyRequest.courseCode) {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: "courseCode is required"
        })
    }

    if (!bodyRequest.courseName) {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: "courseName is required"
        })
    }

    if (!bodyRequest.duration) {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: "duration is required"
        })
    }

    if (!bodyRequest.level) {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: "level is required"
        })
    }

    if (!bodyRequest.coverImage) {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: "coverImage is required"
        })
    }

    if (!bodyRequest.teacherName) {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: "teacherName is required"
        })
    }

    if (!bodyRequest.teacherPhoto) {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: "teacherPhoto is required"
        })
    }


    if (!(Number.isInteger(bodyRequest.price) && bodyRequest.price > 0)) {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: "No Student is not valid"
        })
    }

    if (!(Number.isInteger(bodyRequest.discountPrice) && bodyRequest.discountPrice >= 0)) {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: "No Student is not valid"
        })
    }

    //B3: Thao tác với cơ sở dữ liệu
    let createCourse = {
        _id: mongoose.Types.ObjectId(),
        courseCode: bodyRequest.courseCode,
        courseName: bodyRequest.courseName,
        price: bodyRequest.price,
        discountPrice: bodyRequest.discountPrice,
        duration: bodyRequest.duration,
        level: bodyRequest.level,
        coverImage: bodyRequest.coverImage,
        teacherName: bodyRequest.teacherName,
        teacherPhoto: bodyRequest.teacherPhoto,
        isPopular: bodyRequest.isPopular,
        isTrending: bodyRequest.isTrending
    }

    courseModel.create(createCourse, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal sever error",
                message: error.message
            })
        } else {
            response.status(201).json({
                status: "Success: Course Created",
                data: data
            })
        }
    })
}

//get all Course
const getAllCourse = (request, response) => {
    //B1: Thu thập dữ liệu
    //B2: Kiểm tra dữ liệu
    //B3: Thao tác với cơ sở dữ liệu
    courseModel.find((error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal sever error",
                message: error.message
            })
        } else {
            response.status(201).json({
                status: "Success: Course All",
                data: data
            })
        }
    })
}

const getCourseById = (request, response) => {
    //B1: Thu thập dữ liệu
    let courseId = request.params.courseId;

    //B2: Kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(courseId)) {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: "Course ID is not valid"
        })
    }

    //B3: Thao tác với cơ sở dữ liệu
    courseModel.findById(courseId, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal sever error",
                message: error.message
            })
        } else {
            response.status(200).json({
                status: "Success: Course By ID ",
                data: data
            })
        }
    })
}

const updateCourseById = (request, response) => {
    //B1: Thu thập dữ liệu
    let courseId = request.params.courseId;
    let bodyRequest = request.body;

    //B2: Kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(courseId)) {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: "Course ID is not valid"
        })
    }

    //B3: Thao tác với cơ sở dữ liệu
    let courseUpdate = {
        courseCode: bodyRequest.courseCode,
        courseName: bodyRequest.courseName,
        price: bodyRequest.price,
        discountPrice: bodyRequest.discountPrice,
        duration: bodyRequest.duration,
        level: bodyRequest.level,
        coverImage: bodyRequest.coverImage,
        teacherName: bodyRequest.teacherName,
        isPopular: bodyRequest.isPopular,
        isTrending: bodyRequest.isTrending
    }

    courseModel.findByIdAndUpdate(courseId, courseUpdate, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal sever error",
                message: error.message
            })
        } else {
            response.status(201).json({
                status: "Success: Course Update",
                data: data
            })
        }
    })
}

const deleteCourseById = (request, response) => {
    //B1: Thu thập dữ liệu
    let courseId = request.params.courseId;

    //B2: Kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(courseId)) {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: "Course ID is not valid"
        })
    }

    //B3: Thao tác với cơ sở dữ liệu
    courseModel.findByIdAndDelete(courseId, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal sever error",
                message: error.message
            })
        } else {
            response.status(204).json({
                status: "Success: Delete course success"
            })
        }
    })

}

//   Export controller thành 1 module là 1 object
module.exports = {
    createCourse: createCourse,
    getAllCourse: getAllCourse,
    getCourseById: getCourseById,
    updateCourseById: updateCourseById,
    deleteCourseById: deleteCourseById,
}

// {
//     "courseCode": "NodeJs",
// "courseName": "Backend JavaScript",
// "price": 20,
// "discountPrice": 5,
// "duration": "60 ngày",
// "level":"inter",
// "coverImage": "images/NodeJs",
// "teacherName": "Lê Mạnh Tuấn",
// "teacherPhoto":"images/Teacher"
// }